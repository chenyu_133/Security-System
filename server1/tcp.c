#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>

#include "zigbee.h"
#include "tcp.h"
#include "uart1.h"
#define MAX_BACKLOG	1024


void *actionSelect(void *arg);

int LEN = 0;
int ledflag2 = 0;
int manue = 0;
ENV goal = {
	.temp  = 30,
	.hum   = 30,
	.light = 90};
extern int zigbeeFd;
void *resetValue(int fd,char **argv);
typedef void *(*function)(int,char **);
void *change_statue(int fd,char **argv);
void *pictureKey(int fd,char **argv);
extern void *pictureUpdate(int fd,char **argv);

typedef struct func 
{
	char *str;
	function Rf;
}FUNC;

FUNC commondSet[] = {
	{"environment",zigbeeFunc},
};
/****************************************************************************************
* Function Name  : sqlite		             										    *
* Description    : select card form record list.										*
* Input          : arg :the arry where store card number  	 							*
* Output         : None																	*
* Return         : None																	*
****************************************************************************************/

int tcp_server_init(const char *ip,const char *port)
{
	int ret;
	int listenfd;
	int opt = 1;
	struct sockaddr_in srvaddr;
	
	memset(&srvaddr,0,sizeof(struct sockaddr_in));
	srvaddr.sin_family = AF_INET;
	
	if(ip != NULL)
	{
		ret = inet_pton(AF_INET,ip,&srvaddr.sin_addr);
		if(ret != 1)
		{
			fprintf(stderr,"server ip is err\n");
			return -1;
		}
	}
	else
	{
		srvaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	}
	
	if (port != NULL)
		srvaddr.sin_port = htons(atoi(port));
	else 
	{
		fprintf(stderr, "server->port: port must be assigned\n");
		return -1;
	}
	
	listenfd = socket(AF_INET,SOCK_STREAM,0);
	if(listenfd == -1)
	{
		perror("listenfd");
		return -1;
	}
	setsockopt(listenfd,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt));
	
	ret = bind(listenfd,(struct sockaddr*)&srvaddr,sizeof(struct sockaddr_in));
	if(ret <0)
	{
		perror("listen");
		return -1;
	}
	
	ret =listen(listenfd,MAX_BACKLOG);
	if(ret <0)
	{
		perror("listen");
		return -1;
	}
	return listenfd;
}

/****************************************************************************************
* Function Name  : sqlite		             										    *
* Description    : select card form record list.										*
* Input          : arg :the arry where store card number  	 							*
* Output         : None																	*
* Return         : None																	*
****************************************************************************************/

void wait_connect(int listenfd)
{
	assert(listenfd > 0);
	int ret;
	int acceptfd;
	pthread_t pthread_action;
	while(1)
	{
		acceptfd = accept(listenfd, NULL, NULL);
		if(acceptfd < 0)
		{
			perror("accept error.");
			continue;
		}
		printf("client accept success!\n");
		ret = pthread_create(&pthread_action, NULL, actionSelect, acceptfd);
		if(0 != ret)
		{
			perror("create pthread_action error.");
			close(acceptfd);
			continue;
		}
	}
}

void *actionSelect(void *arg)
{
	assert(NULL != arg);
	char buf[BUF_SIZE];
	char *argv[20] = {0};
	int acceptfd = (int)arg;
	int ret,i;
	while(1)
	{
		memset(buf,0,BUF_SIZE);
		ret = recv(acceptfd, buf, BUF_SIZE, 0);
		if(ret <= 0)
		{
			close(acceptfd);
			pthread_exit("hello");
		}
		puts(buf);
		memset(argv,0,20);
		ret = getorder(buf,argv);
		
		printf("argv[0] = %s\r\n",argv[0]);
		for(i = 0;i < sizeof(commondSet)/sizeof(FUNC);i++)
		{
			printf("turn to compare with %s\r\n",commondSet[i].str);
			if(NULL != strstr(argv[0],commondSet[i].str))
			{
				printf("RESULT = %s\r\n",commondSet[i].str);
				
				commondSet[i].Rf(acceptfd,argv);
				break;
			}
		}
		
	}
}

/****************************************************************************************
* Function Name  : sqlite		             										    *
* Description    : select card form record list.										*
* Input          : arg :the arry where store card number  	 							*
* Output         : None																	*
* Return         : None																	*
****************************************************************************************/

int getorder(char *buf,char **argv)
{
	assert(NULL != buf);
	assert(NULL != argv);
    char *p;
    p = strtok(buf,",");
    int i = 0;
    while(NULL != p)
    {
        argv[i] = p;
        p = strtok(NULL,",");
        argv[i++];
    }
    return 1;
}

/************************************* End of File *****************************************/
