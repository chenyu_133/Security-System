#ifndef _BUZZER_H
#define _BUZZER_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include "fspwm.h"

#define ZGB_DATA_SIZE	100
#define ARRAY_SIZE(a)	(sizeof(a) / sizeof(a[0]))
#define DO	262
#define RE	294
#define MI	330
#define FA	349
#define SOL	392
#define LA	440
#define SI	494

#define BEAT	(60000000 / 120)




int buzzer_func();

#endif