#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <ctype.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/fb.h>

#include "tcp.h"


struct fb_var_screeninfo vinfo;
#define FILEPATH_MAX (80)

int listenfd;
int zigbeeFd;

/****************************************************************************************
* Function Name  : sqlite		             										    *
* Description    : select card form record list.										*
* Input          : arg :the arry where store card number  	 							*
* Output         : None																	*
* Return         : None																	*
****************************************************************************************/

int main(int argc,char **argv)
{
	int ret;
	listenfd = tcp_server_init("192.168.1.127", "8888");
	if (listenfd == -1)
	{
		exit(EXIT_FAILURE);
	}
	else
	{
		fprintf(stdout, "init server success\n");
	}
	
	AA:	zigbeeFd = UART0_Open(zigbeeFd); //打开串口，返回文件描述符  
	if (zigbeeFd < 0)
	{
		sleep(1);
		goto AA;
	}
	else
	{
		fprintf(stdout, "init zigbee success\n");
	}
    do
	{  
		ret = UART0_Init(zigbeeFd,115200,0,8,1,'N');  
		printf("Set Port Exactly!\n");  
	}while(-1 == ret || -1 == zigbeeFd);
	

	wait_connect(listenfd);
	
	close(listenfd);
	exit(EXIT_SUCCESS);
}


