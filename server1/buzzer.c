#include "buzzer.h"
#include "fspwm.h"
int buzzer_func()
{
	int i;
	int fd;
	int ret;
	unsigned int freq;
	fd = open("/dev/pwm", O_RDWR);
	if (fd == -1)
	{	
		printf("open fail~\n");
		goto fail;
	}
	ret = ioctl(fd, FSPWM_START);
	if (ret == -1)
		goto fail;

	for (i = 0; i < 10000; i++) {
		ret = ioctl(fd, FSPWM_SET_FREQ, 1000);
		if (ret == -1)
			goto fail;
		usleep(1000);
	}

	ret = ioctl(fd, FSPWM_STOP);
	if (ret == -1)
		goto fail;
	return 0;
fail:
	perror("pwm test");
	exit(EXIT_FAILURE);
}

