#include <stdio.h>
#include <pthread.h>
#include "zigbee.h"
#include "uart1.h"
#include "tcp.h"
#include "buzzer.h"

int fanflag = 0;
int ledflag = 0;
extern pthread_mutex_t env_mutex;
extern int zigbeeFd;
extern int sel_res;
extern int manue;
extern ENV goal;
void QTshow(int,unsigned char buf[]);
void sqlite(unsigned char buf[]);

/****************************************************************************************
* Function Name  : thread_zigbee             										    *
* Description    : Zigbee thread exec function.											*
* Input          : arg :socket return value												*
* Output         : None																	*
* Return         : None																	*
****************************************************************************************/

void zigbee_get_dat(unsigned char *buf)
{
	assert(NULL != buf);
	int i;
	printf("zigbee_get_dat\r\n");
	
	test(zigbeeFd,buf);
	
	for(i=0;i<36;i++)
	{
		printf("%x ",(unsigned char)buf[i]);
		puts(buf);
	}
	printf("\r\n");
}

/****************************************************************************************
* Function Name  : thread_zigbee             										    *
* Description    : Zigbee thread exec function.											*
* Input          : arg :socket return value												*
* Output         : None																	*
* Return         : None																	*
****************************************************************************************/

void *zigbeeFunc(int connfd,char **argv)
{
	int ret;
	int j = 0, k = 0, i = 0;
    char *argv1[32] = {0};
    char *gas;
    char *dw;
    char *inf;
	printf("fd3 = %d\r\n",connfd);
	unsigned char buf[ZGB_DATA_SIZE];
	while(1)
	{
		test(zigbeeFd,buf);
	
		QTshow(connfd,buf);

		while(buf[i] != '\0')
		{
			if(buf[i] == '#')
			{
				buf[i] = '\0';
				argv1[j] = buf + k;
				j++;
				k = i + 1;
			}
			i++;
		}
		argv1[j] = buf + k;
		gas = argv1[0];
		dw = argv1[1];
		inf = argv1[2];
		if(*gas == 'y' || *dw == 'y' || *inf == 'y')
		{
			buzzer_func();
			continue;
		}	
	}
	printf("\r\n");
}

/****************************************************************************************
* Function Name  : QTshow		             										    *
* Description    : send temp,hum,light to QT.											*
* Input          : arg :the arry where store temp,hum,light 							*
* Output         : None																	*
* Return         : None																	*
****************************************************************************************/

void QTshow(int connfd,unsigned char buf[])
{
	assert(NULL != buf);
	assert(0 < connfd);
	int ret;
	ret = send(connfd,buf,strlen(buf));
}
