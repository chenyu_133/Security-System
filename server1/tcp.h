#ifndef __TCP_H__
#define __TCP_H__

#define BUF_SIZE 200

typedef struct envValue
{
	int temp;
	int hum;
	int light;
}ENV;

int tcp_server_init(const char *, const char *);
void wait_connect(int);
void seal_with(int);
ssize_t tcp_server_recv(int , void *, size_t);
ssize_t tcp_server_send(int connfd, const void *, size_t);
ssize_t tcp_server_recv_exact_nbytes(int , void *, size_t);
ssize_t tcp_server_send_exact_nbytes(int , const void *, size_t);
int tcp_server_disconnect(int);
int tcp_server_exit(int);
int getorder(char *,char **);

#endif
