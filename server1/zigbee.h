#ifndef __MODUL_H__
#define __MODUL_H__

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <termios.h>
#include <error.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>

#define ZGB_DATA_SIZE	100
#define REQ_DATA_SIZE   32
#define HDR_DATA_SIZE   128
#define ZIGBEE_DEV_PATH "/dev/ttyUSB0"
void *zigbeeFunc(int fd,char **argv);
int zigbee_init(char *devpath, int baudrate);
void zigbee_get_dat(unsigned char *);
int zigbee_put_cmd(int fd, unsigned char *p);
int zigbee_exit(int fd);

#endif
