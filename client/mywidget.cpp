#include "mywidget.h"
#include "ui_mywidget.h"
#include "QMessageBox"
mywidget::mywidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mywidget)
{
    ui->setupUi(this);
    this->setWindowTitle("login");
    this->setWindowIcon(QIcon(":/inter.PNG"));
    ui->pswrd->setEchoMode(QLineEdit::Password);
    mywidget::Initmydb();
}

mywidget::~mywidget()
{
    database.close();
    delete ui;
}

void mywidget::on_login_clicked()
{
    QString c = ui->user->text();
    QString d = ui->pswrd->text();
    if(select(c,d)==1)
    {
        QMessageBox::information(this,"登录","登录成功!");
        this->close();
        spypag.show();
    }
    else if(ui->user->text().isEmpty() == true || ui->pswrd->text().isEmpty() == true)
    {
        QMessageBox::warning(this,"警告","用户名或密码不能为空!");
    }
    else
    {
        QMessageBox::warning(this,"警告","用户名或密码错误!");
    }
    ui->pswrd->clear();
}

void mywidget::on_cancle_clicked()
{
    this->close();
}

void mywidget::on_register_2_clicked()
{
    QString a = ui->user->text();

    QString b = ui->pswrd->text();
    mywidget::insert(a,b);
}


void mywidget::Initmydb()
{

    database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName("MyUser");

    if(!database.open())
    {
        qDebug() << "Error : Failed to connect database."<<database.lastError();
    }
    else
    {

        QSqlQuery sql_query;
        QString strsql = QString::fromLocal8Bit("create table if not exists User( name varchar(100), password varchar(100));");
        if(!sql_query.exec(strsql))
        {
            QSqlError str = sql_query.lastError();
            QString strerror = str.text();
            QMessageBox::information( NULL, QString::fromLocal8Bit("error"),strerror);
        }

    }

}


void mywidget::insert(QString name,QString password)
{
    QSqlQuery sql_query;
    sql_query.prepare("insert into User(name,password) values(:name,:password)");
    sql_query.bindValue(":name", name);
    sql_query.bindValue(":password",password);
    if(!sql_query.exec())
    {
        qDebug() << sql_query.lastError();
        QMessageBox::information(this,"注册","用户名已存在!");
    }
    else
    {
        QMessageBox::information(this,"注册","注册成功!");
    }

}

int mywidget::select(QString str1,QString str2)
{
    QString name;
    QString password;
    QSqlQuery sql_query;
    QString select_sql = "select name, password from User";
    if(!sql_query.exec(select_sql))
    {
        qDebug()<<sql_query.lastError();
    }
    else
    {
        while(sql_query.next())
        {
            name = sql_query.value(0).toString();
            password = sql_query.value(1).toString();
            qDebug()<<QString("name:%10    password:%10").arg(name).arg(password);
        }
    }
    if((QString::compare(str1,name)==0)&&(QString::compare(str2,password)==0))
    {
        return 1;
    }
    else
        return 0;
}
