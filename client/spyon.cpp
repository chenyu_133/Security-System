#include "spyon.h"
#include "ui_spyon.h"
#include <QHostAddress>
#include <QDebug>
#include <QMessageBox>
#include <QByteArray>
#include <QDateTime>
#include <QPainter>
#include <QImage>
spyon::spyon(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::spyon)
{
    ui->setupUi(this);
    socket = new QTcpSocket(this);
    socket2 = new QTcpSocket(this);
    pixmap = new QPixmap();
    timer3 = new QTimer(this);
    connect(timer3,SIGNAL(timeout()),this,SLOT(ShowTime()));
    timer3->start(1000);
    connect(socket,SIGNAL(connected()),this,SLOT(connectedSlot()));
    connect(socket2,SIGNAL(connected()),this,SLOT(msconnected()));
    connect(socket,SIGNAL(readyRead()),this,SLOT(recvDataSlot()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnectedSlot()));
}

spyon::~spyon()
{
    delete ui;


    ui->pushButton->setEnabled(true);
    timer->stop();
    this->socket->close();


    this->socket2->close();
}


void spyon::connectedSlot()
{
    qDebug() << "ENV connected";
    ui->pushButton->setEnabled(false);
    timer2 = new QTimer(this);
    connect(timer2,SIGNAL(timeout()),this,SLOT(updateenv()));
    timer2->start(1000);
}


void spyon::updateenv()
{
    QString str = "environment";
    QByteArray arr;
    arr.append(str);
    socket->write(arr);
}


void spyon::recvDataSlot()
{
    QByteArray arr = socket->readAll();
    QString env = QString(arr);
    QString gas = env.section("#",0,0);
    if(gas == QString::fromLocal8Bit("y"))
    {
        ui->gas->clear();
        ui->gas->setText("泄漏");
        ui->label_6->setPixmap(QPixmap(":/red.png"));
    }
    else if(gas == QString::fromLocal8Bit("n"))
    {
        ui->gas->clear();
        ui->gas->setText("安全");
        ui->label_6->setPixmap(QPixmap(":/green.png"));
    }
    QString DW = env.section("#",1,1);
    if(DW == QString::fromLocal8Bit("y"))
    {
        ui->DW->clear();
        ui->DW->setText("开启");
        ui->label_7->setPixmap(QPixmap(":/red.png"));
    }
    else if(DW == QString::fromLocal8Bit("n"))
    {
        ui->DW->clear();
        ui->DW->setText("关闭");
        ui->label_7->setPixmap(QPixmap(":/green.png"));
    }
    QString inf = env.section("#",-1,-1);
    if(inf == QString::fromLocal8Bit("y "))
    {
        ui->lineEdit->clear();
        ui->lineEdit->setText("有人");
        ui->label_8->setPixmap(QPixmap(":/red.png"));
    }
    else if(inf == QString::fromLocal8Bit("n "))
    {
        ui->lineEdit->clear();
        ui->lineEdit->setText("无人");
        ui->label_8->setPixmap(QPixmap(":/green.png"));
    }
    qDebug()<<"gas ="<<gas;
    qDebug()<<"DW ="<<DW;
    qDebug()<<"inf ="<<inf;
}

void spyon::on_pushButton_clicked()
{
    socket->connectToHost(QHostAddress("192.168.1.127"), 8888);
    socket2->connectToHost(QHostAddress("192.168.1.127"), 8888);
}

void spyon::msconnected()
{
    qDebug() << "PIC connected";
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(updatepic()));
    timer->start(100);
}



void spyon::updatepic()    //图片以字符串方式发送
{
    int ret;          //每次接收到的大小
    int total = 0;    //已接收到的大小
    char *request = "picture";
    QString response;
    unsigned int piclen;   //记录图片总大小信息
    char picbuf[1024 * 1024];

    ret = socket2->write(request, strlen(request));

    if (ret != strlen(request))
    {
		qDebug() << "send request failed";
		timer->stop();
        socket2->close();
    }                      //若发送失败，关闭socket2;

    socket2->flush();       //清空缓存，发送给服务端
    socket2->waitForReadyRead(30000);   //实现阻塞，有数据就返回或者阻塞30秒返回

    QByteArray arr = socket2->readAll();

	QString str2 = QString(arr);

    response = str2.section("#",1,1);  //获取包含图片大小的string
    piclen = response.toInt();   //将图片大小的字符串转为int
    qDebug() <<piclen;
    while (total < piclen)                    //已接收大小小于图片总大小循环
    {
        ret = socket2->read(picbuf+total, piclen-total);
        if (ret < 0)

        {
            qDebug() << "recv pic failed" << ret;
            timer->stop();
            socket2->close();
            socket2 = NULL;
            return;
        }
        else if (ret == 0)
        {
            socket2->waitForReadyRead(30000);
            continue;
        }
        else
		{
            total += ret;
		}
    }
   pixmap->loadFromData((uchar *)picbuf, piclen,"JPEG");
   ui->label_4->setPixmap(*pixmap);
}

void spyon::ShowTime()
{
    QDateTime timer3 = QDateTime::currentDateTime();
    QString str = timer3.toString("yyyy-MM-dd hh:mm:ss dddd");
    ui->time->setText(str);
}

void spyon::disconnectedSlot()
{
    ui->pushButton->setEnabled(true);
}





