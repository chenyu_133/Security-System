#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>
#include "spyon.h"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QImage>
#include <QPainter>
namespace Ui {
class mywidget;
}


class mywidget : public QWidget
{
    Q_OBJECT

public:
    explicit mywidget(QWidget *parent = 0);
    ~mywidget();

private slots:
    void on_login_clicked();

    void on_cancle_clicked();

    void on_register_2_clicked();

private:
    Ui::mywidget *ui;
    spyon spypag;

    QSqlDatabase database;
    void Initmydb();
    void insert(QString name,QString password);
    int select(QString name,QString password);

};
#endif // MYWIDGET_H
