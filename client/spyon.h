#ifndef SPYON_H
#define SPYON_H
#include <QTimer>
#include <QWidget>
#include <QPixmap>
#include <QTcpSocket>
#include <QString>

namespace Ui {
class spyon;
}

class spyon : public QWidget
{
    Q_OBJECT

public:
    explicit spyon(QWidget *parent = 0);
    ~spyon();

private slots:
    void recvDataSlot();
    void on_pushButton_clicked();  
    void ShowTime();
    void disconnectedSlot();
    void connectedSlot();
    void msconnected();
    void updatepic();
    void updateenv();

private:
    Ui::spyon *ui;
    QTcpSocket *socket;
    QTcpSocket *socket2;
    QTimer  *timer;
     QTimer  *timer2;
     QTimer *timer3;
    QString Usermag;
    QString speed;
    QString curflag;
    QString fanflag;
    QPixmap *pixmap;
};

#endif // SPYON_H
