/****************************************************************************************
* File   : main.c             														    *
* Author : Farsight Design Team										                    *
* Version: V1.00								                                        *
* Date   : 2018.10.21                                                                   *
* Brief  : Main program body                                                            *
****************************************************************************************/	

/****************************************************************************************
* History   :                                                                           *
* 2011.05.10:  V1.00	initial version	                                                *
****************************************************************************************/	

/*------------------------------------Includes-----------------------------------------*/
#include <stdio.h>
#include <string.h>	
#include "lpc11xx.h"
#include "clkconfig.h"
#include "gpio.h"
#include "uart.h"
#include "timer32.h"
#include "ssp.h"
#include "i2c.h"
#include "wdt.h"
#include "adc.h"
#include "rom_drivers.h"

#include "seg7led.h"
#include "oled2864.h" 
#include "light.h"
#include "acc.h"
#include "key.h"
#include "rfid.h"
#include "tools.h"
#include "spi_uart.h"
#include "temp_hum.h"
#include "collect_data.h"
#include "led_spk_fan.h"
#include "myrfid.h"
#include "menu.h"
#include "data_type.h"
#include "file_operation.h"
#include "sys_init.h"

uint8_t 		STORAGE_NUM = 0;  //设备号（发送开机命令后由上层节点返回）
message_tag_t   message_tag_s;
com_t           com_s;
tem_t 			tem_s;
hum_t 			hum_s;
gas_t       gas_s;
door_t      door_s;
inf_t        inf_s;
light_t 		light_s;
//acc_t 			acc_s;
adc_t       	adc_s;
state_t 		state_s;
//rfid_t 			rfid_s;
command_t 		command_s;
key_t 			key_s;
env_msg_t 		env_msg_s;
data_t			data_s;
message_t 		message_s;
message_t  		message_r;
 	  	
volatile uint32_t counter1 = 0;
volatile uint32_t counter2 = 0;
static uint8_t		   cnt = 3;

extern char zigbee_flag;
extern char rfid_flag;
extern char up_flag;
extern char down_flag;
extern char left_flag;
extern char right_flag;
extern char esc_flag;
extern char sel_flag;
extern uint8_t numb;
extern uint8_t	SPI752_rbuf_1[];
int dark = 0;
uint8_t  k = 0;
uint8_t  j = 0;
uint8_t	 rx[48] = {0};
uint8_t  rbuf[48] = {0};
uint8_t  *p;
uint32_t rfid_id;

char 	 dis_buf[30] = {0};
char buf[37];


/****************************************************************************************
* Function Name  : SysTick_Handler             										    *
* Description    : SysTick interrupt Handler.											*
* Input          : None																	*
* Output         : None																	*
* Return         : None																	*
****************************************************************************************/	
void SysTick_Handler(void)
{
	static uint32_t	Tick=0;
  
	Tick ++;
	if(Tick == 500)
	{
		GPIOSetValue(PORT3, 1, 0);		// PIO3_1 output 1, turn on LED2
		cnt ++;
		cnt &= 0x1f;
	}
	else if(Tick >= 1000)
	{
		counter1++;
		counter2++;
		GPIOSetValue(PORT3, 1, 1);		// PIO3_1 output 1, turn off LED2
		Tick = 0;
	}
}

/******************************************************************************************
* Function Name  : main             													  *
* Description    : Main program.														  *
* Input          : None																	  *
* Output         : None																	  *
* Return         : None																	  *
******************************************************************************************/	
int main(int argc,char **argv)
{
	p = rx;
	init();					
	/**************************************************************************************
	*	section:  																		  *
	*           sending message(the environment message or the rfid message)			  *
	*		    receive message from the master and give matching method.			      *
	* description:																	      *
	*			designed to send message to master every three seconds.Time is controlled *
	*			by the SysTick().														  *
	*			receive message is triggered by the interrupt.According to the treaty,    *
	*			slave operates correctly.												  *
	**************************************************************************************/				
	STORAGE_NUM = 8;
	while(1)
    {	
			
			/*if(rfid_flag)
		{
			OLED_ClearScreen();
			snprintf(buf, 17, "show message");
			OLED_DisStrLine(1, 0, (uint8_t *)buf);
			rfid_flag = 0;  
			if(Rfid_Operation(STORAGE_NUM,rbuf))
			{  
				memset(&com_s,com_len,0);
				com_s.a='c'; 
				com_s.b='a';			
				memcpy(&com_s.str,rbuf+2,sizeof(rbuf));
				
				snprintf(buf, 17, "%s",com_s.str);
				OLED_DisStrLine(2, 0, (uint8_t *)buf);				
				//send_card_zigbee(&com_s);					
			} 
			GPIOIntEnable(PORT2,8);
			//OLED_ClearScreen();
		}*/
		
		if(counter1 > 1)
	 	{
			memset(&message_s,message_len,0);
			collect_data(&tem_s,&hum_s,&light_s,&gas_s,&door_s,&inf_s);
			adc_ret(&adc_s);
	    	//acc_ret(&acc_s);	
			get_state(&state_s);
			//fill_env(&env_msg_s,tem_s,hum_s,acc_s,adc_s,light_s,state_s);	 
			//fill_data(&data_s,data_e,NULL,NULL,&env_msg_s,NULL); 
			//fill_message(&message_s,data_flow,STORAGE_NUM,message_len,&data_s,0);
			//send_message_zigbee(&message_s); 	
			fill_env(&env_msg_s,tem_s,hum_s,adc_s,light_s,state_s);	 
			fill_data(&data_s,data_e,NULL,&env_msg_s); 
			fill_message(&message_s,data_flow,STORAGE_NUM,message_len,&data_s,0);
			//send_message_zigbee(&message_s); 
			send_message_zigbee(&gas_s.gas,&door_s.door,&inf_s.inf); 
			
			counter1 = 0;
		}
		if(counter2 > 2)
		{
			WDTFeed();
			counter2 = 0;
		}	
		/*memcpy(&message_r,SPI752_rbuf_1,36);
		if ((message_r.tag == command) && (message_r.slave_address == STORAGE_NUM))
		{
			switch (message_r.data.command.operate_id)
			{
				case on_led1:
					if(1 != dark)
					{
						OLED_ClearScreen();
						snprintf(buf, 17, "it is too dark");
						OLED_DisStrLine(1, 0, (uint8_t *)buf);
					}
					dark = 1;
					GPIOSetValue(PORT3, 0, 0);		 //turn on led1
					break;
				case off_led1:
					if(0 != dark)
					{
						OLED_ClearScreen();
						snprintf(buf, 17, "good sunshine");
						OLED_DisStrLine(1, 0, (uint8_t *)buf);
					}
					dark = 0;
					GPIOSetValue(PORT3, 0, 1);			 //turn off led1
					break;
				case on_fan:
					GPIOSetValue(PORT0, 2, 0);
					break;
				case off_fan:
					GPIOSetValue(PORT0, 2, 1);
					Seg7Led_Put(' ');
					break;
				case on_fan_low:
					Seg7Led_Put(1);
					break;
				case on_fan_mid:
					Seg7Led_Put(2);
					break;
				case on_fan_high:
					Seg7Led_Put(3);
					break;
				case on_speaker:
					speaker_op(0);
					speaker_op(1);
					break;
				case off_speaker:
					speaker_op(0);
					break;
				case off_machine:
					GPIOSetValue(PORT3, 0, 1);
					GPIOSetValue(PORT0, 2, 1);
					Seg7Led_Put(' ');
					speaker_op(0);
					OLED_ClearScreen();
					snprintf(dis_buf, 16, "I'm died");
					OLED_DisStrLine(1, 0, (uint8_t *)dis_buf);
					SysTick->CTRL  = ~(SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk);
					
					break;								
				default:
					printf("not write command");
					//break;
			}
		}
		else if((message_r.tag == command) && (message_r.slave_address == BROADCAST))
		{
			if(message_r.data.command.operate_id == off_machine)
			{
				GPIOSetValue(PORT3, 0, 1);
				GPIOSetValue(PORT0, 2, 1);
				Seg7Led_Put(' ');
				speaker_op(0);
				OLED_ClearScreen();
				snprintf(dis_buf, 16, "I'm died");
				OLED_DisStrLine(1, 0, (uint8_t *)dis_buf);
				SysTick->CTRL  = ~(SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk  | SysTick_CTRL_ENABLE_Msk);
				for(;;){;}	
			}
		}*/
		/*if(up_flag)
		{
			up_flag = 0;
			fill_key(&key_s,key_up);
				 
			fill_data(&data_s,data_e,NULL,&env_msg_s); 
			fill_message(&message_s,data_flow,STORAGE_NUM,message_len,&data_s,0);
			
			send_message_zigbee(&message_s);
		}
		if(down_flag)
		{
			down_flag = 0;
			fill_key(&key_s,key_down);
			fill_data(&data_s,data_e,NULL,&env_msg_s); 
			fill_message(&message_s,data_flow,STORAGE_NUM,message_len,&data_s,0);
			send_message_zigbee(&message_s);
		}
		if(left_flag)
		{
			left_flag = 0;
			fill_key(&key_s,key_left);
			fill_data(&data_s,data_e,NULL,&env_msg_s); 
			fill_message(&message_s,data_flow,STORAGE_NUM,message_len,&data_s,0);
			send_message_zigbee(&message_s);		
		}
		if(right_flag)
		{
		 	right_flag = 0;
			fill_key(&key_s,key_right);
			fill_data(&data_s,data_e,NULL,&env_msg_s); 
			fill_message(&message_s,data_flow,STORAGE_NUM,message_len,&data_s,0);	
			send_message_zigbee(&message_s);
		}
		if(sel_flag)
		{
			sel_flag = 0;
			fill_key(&key_s,key_sel);
			fill_data(&data_s,data_e,NULL,&env_msg_s); 
			fill_message(&message_s,data_flow,STORAGE_NUM,message_len,&data_s,0);
			send_message_zigbee(&message_s);
		}
		if(esc_flag)
		{
			esc_flag = 0;
			fill_key(&key_s,key_esc);
			fill_data(&data_s,data_e,NULL,&env_msg_s); 
			fill_message(&message_s,data_flow,STORAGE_NUM,message_len,&data_s,0);
			send_message_zigbee(&message_s);
		}*/
		
		
		
		
		__WFI(); 	 
    }
}

/************************************* End of File *****************************************/
